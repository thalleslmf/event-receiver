const express = require('express')
const app = express()
const port = 3000
const bodyParser = require('body-parser')

app.use(bodyParser.json())

let demoLogger = (req, res, next) => {
    let current_datetime = new Date();
    let formatted_date =
        current_datetime.getFullYear() +
        "-" +
        (current_datetime.getMonth() + 1) +
        "-" +
        current_datetime.getDate() +
        " " +
        current_datetime.getHours() +
        ":" +
        current_datetime.getMinutes() +
        ":" +
        current_datetime.getSeconds();
    let method = req.method;
    let url = req.url;
    let status = res.statusCode;
    let log = `[${formatted_date}] ${method}:${url} ${status}`;
    if (req.method !== "GET") {
        console.log(log);
    }
    next();
};
app.use(demoLogger)
app.get('/', (req,res) => {
    res.status(200).send({
        message: 'healthy!'
    })
})

app.post('/pre-rollout', (req,res) => {

    res.status(200).json( {
        message: req.body,

    })
})

app.post('/confirm-rollout', (req,res) => {
    res.status(200).json( {
        message: req.body,

    })
})


app.post('/post-rollout', (req,res) => {
    res.status(200).json( {
        message: req.body,

    })
})

app.post('/confirm-promotion', (req,res) => {
    res.status(200).json( {
        message: req.body,

    })
})

app.post('/confirm-traffic-increase', (req,res) => {
    console.log(req.body)
    res.status(200).json( {
        message: req.body,

    })
})

app.post('/rollback', (req,res) => {
    console.log(req.body)
    res.status(500).json( {
        message: req.body,
    })
})

app.post('/event', (req,res) => {
    console.log(req.body)
    res.status(200).json( {
        message: req.body,
    })
})
app.listen(3000)